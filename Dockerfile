FROM openjdk:8

ENV DATABASE_URL=jdbc:h2:mem:assignment
ENV SERVER_PORT=8090

RUN mkdir /temp
ADD ./target/*.jar /temp/assignment.jar
ADD run_app.sh /run_app.sh
RUN chmod 777 /run_app.sh

ENTRYPOINT ["/run_app.sh"]