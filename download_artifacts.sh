job_id=$1
access_token=$2
project_id=$3
branch_name=$4

#curl --output "./artifacts/artifacts.zip" --header "PRIVATE-TOKEN: $access_token" "https://gitlab.com/ps.mutaz.maraqa/devops-practices/-/jobs/$job_id/artifacts/download?file_type=archive" -L
curl --output "./artifacts/artifacts.zip" --header "PRIVATE-TOKEN: $access_token" "https://gitlab.com/api/v4/projects/$project_id/jobs/artifacts/$branch_name/download?job=build" -L