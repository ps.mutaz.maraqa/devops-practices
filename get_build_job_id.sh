access_token=$1
project_id=$2
pipeline_id=$3

echo $(curl --header "PRIVATE-TOKEN: $access_token" "https://gitlab.com/api/v4/projects/$project_id/pipelines/$pipeline_id/jobs?scope[]=success" | jq -r '.[0].id')